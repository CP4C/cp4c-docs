This template is used to report issues and bugs that you have identified in the code, where you know where the issue is and why it is occuring. 

- To request new features, use the feature_request_template
- To ask for assitance with a crash, using the assistance_request_template

To report a bug or issue, you must be able to explain the problem, provide list/output information and refer to an example run which experiences the issue AND point to where in the code the issue lies.

Before starting a new issue, double check that the issue is not already listed either in the issue tracker or
in the wiki FAQ.

## Name of your CP4C project

## Describe the issue

- Provide your explanation of what you think the problem is
   - Include where in the process you hit the issue (e.g. during compilation)

- Show where in the code the issue occurs, and describe why it is wrong
   - link to code in gitlab or paste code snippets here providing the details of routine etc