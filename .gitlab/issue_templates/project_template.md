This template is used to document planned runs using CanESM within the research community. This allows us to document current projects, primary investigators, ECCC points of contact as well as required resources, new capabilities needed and timelines.

This list will be reviewed on a regular basis by the Scientific Steering Committee.

## Project name:

## Main investigators: 

#### Main ECCC points of contact:

#### Description of experiments:

#### Starting point from existing CMIP catalog:

#### Required new capabilities:

#### Model version:

#### Resources available and required:

#### Timeline for work and any practical constraints: