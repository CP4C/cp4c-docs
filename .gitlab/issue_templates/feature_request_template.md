## Identify the CP4C project
   - The name of the CP4C project (should appear in CP4C project table)

## What are you requesting?

Briefly describe what you are requesting, and why (what are you trying to achieve)

Example of requests:

- A new supported experiment for CP4C (e.g. SSP585)
   - List the experiment name, description

- A new restart file from existing ECCC runs (e.g. restart for an esm-hist run)
 
- A new or modified forcing file (e.g. GHG forcing )

- A new model functionality for CP4C (e.g. spectral nudging / relaxation)

- A new model configuration (e.g. CanESM5-CanOE)

## What is the timeline?

- Describe when you need this feature by

## Notes:

- These issues help us to direct requests to the relevant experts, and to guide workplanning
- Requests will be addressed based on when they are submitted, their urgency and their feasibility / level of effort
- While reasonable effort will be made to fufill requests, there is no guarantee that because a request is made it will be satisfied.