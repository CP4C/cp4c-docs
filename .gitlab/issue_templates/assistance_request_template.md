Use this template to request assitance on how to achieve something with CanESM
or reolving an issue experienced during a run (crash etc)

## Name of your CP4C project

## Decribe your request for assistance

### 1. Describe what you are trying to do with the model but don't know how
   - e.g. 
      - I want to reduce the length of my SSP370 run to end in 2020 instead of 2100 
      - I want to reduce the number of nodes used for the run to three

   - Note, your request might be immediately achievable if the functionality already
     exists. However, if the functionality you are requesting does not exist the
     feature_request_template will need to be completed.

OR

### 2. Report a crash for help with resolution

#### Describe the issue

- Provide your explanation of what you think the problem is
   - Include where in the process you hit the issue (e.g. during compilation)

- Provide output from the listing logs or other error logs which give the error

#### Detailed info:

- Provide a full path to the run experiencing this issue
   - (to receive assistance, you must make the directory/files readable by the CP4C group)

- Provide the imsi command use to setup the run in question