#!/usr/bin/env python

###############################################################################
import os
import sys
import time
import struct
import numpy as np
np.core.set_printoptions(linewidth=200)
###############################################################################

STENCIL = []
# Each horizontal field in a ccc file has an description record (IBUF) followed by the field itself (DATA).
# IBUF record:
STENCIL.append({'label' : 'IBUF start', 'bytes' : 4, 'format' : '>i'}) # size of record in bytes
STENCIL.append({'label' : 'KIND',  'bytes' : 8, 'format' : '8s'}) # type of data (e.g. 'GRID', 'ZONL', ...)
STENCIL.append({'label' : 'TIME',  'bytes' : 8, 'format' : '>2i'}) # timestamp
STENCIL.append({'label' : 'NAME',  'bytes' : 8, 'format' : '8s'}) # field name (e.g. 'PCP')
STENCIL.append({'label' : 'LEVEL', 'bytes' : 8, 'format' : '>2i'}) # vertical level
STENCIL.append({'label' : 'ILG',   'bytes' : 8, 'format' : '>2i'}) # no. of longitudes
STENCIL.append({'label' : 'ILAT',  'bytes' : 8, 'format' : '>2i'}) # no. of latitudes
STENCIL.append({'label' : 'KHEM',  'bytes' : 8, 'format' : '>2i'}) # integer indicating whether data is global (0), NH (1), SH (2), or ocean (3)
STENCIL.append({'label' : 'PACK',  'bytes' : 8, 'format' : '>2i'}) # packing density
STENCIL.append({'label' : 'IBUF stop', 'bytes' : 4, 'format' : '>i'}) # size of record in bytes
# DATA record:
STENCIL.append({'label' : 'DATA start', 'bytes' : 4, 'format' : '>i'}) # size of record in bytes
STENCIL.append({'label' : 'xMin', 'bytes' : 8, 'format' : '>d'}) # minimum value in the field (float)
STENCIL.append({'label' : 'xMax', 'bytes' : 8, 'format' : '>d'}) # maximum value in the field (float)
STENCIL.append({'label' : 'DATA', 'format' : '>i'})  # field, stored as integer for packing but will expand to array of floats
STENCIL.append({'label' : 'DATA stop', 'bytes' : 4, 'format' : '>i'}) # size of record in bytes

IBUF_PARMS = ['KIND', 'TIME', 'NAME', 'LEVEL', 'ILG', 'ILAT', 'KHEM', 'PACK']

def chksme(x):
    '''
    Return unique entries of list x (filter out duplicate entries), preserving list order.
    '''
    return [x[m] for m in filter(lambda m: x[m] not in x[:m], range(len(x)))]


def read(filepath, records='all'):
    '''
    Read records from ccc-format binary file.
    '''
    verbose = False
    lr = [] # list to store records
    n = 0 # count no. of records read
    with open(filepath, 'rb') as f:
        while True:
            n += 1
            end_of_file_reached = f.tell() >= os.fstat(f.fileno()).st_size
            if records in ['all', -1]:
                read_record = not end_of_file_reached
            else:
                assert isinstance(records, int), 'invalid no. of records to read: {}'.format(records)
                read_record = ( n <= records ) and ( not end_of_file_reached )
            if not read_record: break
            if verbose: print('\n * record {} * '.format(n))
            ibuf, params, rec = {}, {}, {}
            for k,d in enumerate(STENCIL):
                label, fmt = d['label'], d['format']
                if label == 'DATA':
                    count = ibuf['ILG']*ibuf['ILAT']
                    ad = np.fromfile(f, dtype=fmt, count=count)
                    pack, xMin, xMax = ibuf['PACK'], params['xMin'], params['xMax']
                    NBITS = 64 / pack
                    print(NBITS, pack, xMax, xMin)
                    XSCAL = (2.**(NBITS-1))/(xMax-xMin)
                    ad = ad/XSCAL + xMin
                    if verbose: print('data array shape: {}, start: {}, end: {}'.format(ad.shape, ad[:3], ad[-3:]))                    
                else:
                    bytes = f.read(d['bytes'])
                    a = struct.unpack(fmt, bytes)[-1]
                    if not isinstance(a, (int,float)): 
                        a = str(a.decode('UTF-8'))
                        a = a.strip()
                    if label in IBUF_PARMS:
                        ibuf[label] = a
                    else:
                        params[label] = a
                    if verbose: print('{} ({}) : {}'.format(label, str(type(a)), a))
            rec = {'ibuf' : ibuf, 'params' : params, 'data' : ad}
            lr.append(rec)
    if verbose: print('\nno. of records read: {}'.format(len(lr)))
    return lr
            
def variables(records, arrays=True):
    '''
    From an list of records produced by read(), group records into variables and return the list of variables.
    '''
    lv = []
    v = {'name' : None}
    for rec in records:
        ibuf = rec['ibuf']
        assert ibuf['NAME'] is not None
        if ibuf['NAME'] != v['name']:
            # if name doesn't match previous record's name, start new variable
            v = {'name' : ibuf['NAME'], 'records' : [],
                 'nlat' : ibuf['ILAT'], 'nlon' : ibuf['ILG'], 'level,time' : []}
            lv.append(v)
        v['records'].append(rec)
        v['level,time'].append( (ibuf['LEVEL'], ibuf['TIME']) )
        assert ibuf['ILAT'] == v['nlat']
        assert ibuf['ILG']  == v['nlon']

    if arrays:
        for v in lv:
            # replace list of records with a single array
            nlat, nlon = v['nlat'], v['nlon']
            v['levels'] = chksme([t[0] for t in v['level,time']])
            v['times'] = chksme([t[1] for t in v['level,time']])
            nlev, ntim = len(v['levels']), len(v['times'])
            nrec = len(v['records'])
            assert nlev*ntim == nrec
            assert len(v['level,time']) == nrec
            shp = (nlat, nlon, nlev, ntim)
            v['dims'] = ['latitude', 'longitude', 'level', 'time']
            ad = np.zeros(shp)
            ad.fill(np.nan)
            v['ibuf'] = {}
            for k, rec in enumerate(v['records']):
                lev, tim = v['level,time'][k]
                assert rec['ibuf']['TIME'] == tim
                assert rec['ibuf']['LEVEL'] == lev
                assert rec['data'].shape == (nlat*nlon,)
                rec['data'].shape = (nlat,nlon)
                (m,t) = v['levels'].index(lev), v['times'].index(tim)
                ad[:,:,m,t] = rec['data']
                v['ibuf'][(m,t)] = rec['ibuf']
            assert not np.any(np.isnan(ad))
            for s in ['records', 'level,time']:
                v.pop(s)
            v['data'] = ad
            for s in ['levels', 'times']:
                v[s] = np.array(v[s])

    return lv

class cccvar():
    '''
    Mimic how cccma_py returns variables.
    '''
    def __init__(self,v):
        self.data = v['data']
        self.dims = v['dims']
        self.ibuf = v['ibuf']
        self.timesteps = v['times']
        self.levels = v['levels']
        self.varname = v['name']
    def load_field(self,t,m):
        # t = time index, m = level index
        # They are ordered (time,level) in the function call because that's the
        # way the cccma_py load_field() function does it. 
        return self.data[:,:,m,t]
    def load_ibuf(self,t,m):
        return self.ibuf[(m,t)]

